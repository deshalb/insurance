-- |
module Insurance.Service.DB
  ( -- *
    acceptQuoteTxn,
    createQuoteTxn,
    discardQuoteTxn,
    retrieveInsurancePolicyTxn,
  )
where

------------------------------------------------------------------------------

import Control.Error.Util
import Control.Monad.Reader
import Control.Monad.Trans.Except
import Data.Time
import qualified Data.UUID.V4 as UUID
import qualified Database.Persist.Postgresql as P
import Insurance.DB.Schema
import Insurance.Service.API.Types
import Insurance.Service.DB.Types
import Insurance.Service.Types
import qualified Insurance.Web.Errors as Errors

------------------------------------------------------------------------------

acceptQuoteTxn ::
  MonadIO m =>
  InsuranceUUID ->
  ReaderT P.SqlBackend m (Either Errors.APIError InsuranceEndpointResponse)
acceptQuoteTxn quoteUUID = do
  now <- liftIO getCurrentTime
  -- TODO(dalp): We could update and get the quoteId at the same time here
  -- with esqueleto.
  P.updateWhere
    [DBInsuranceQuoteUuid P.==. quoteUUID]
    [ DBInsuranceQuoteStatus P.=. QuoteStatus_Accepted,
      DBInsuranceQuoteStatusUpdatedAt P.=. now
    ]
  P.selectFirst [DBInsuranceQuoteUuid P.==. quoteUUID] [] >>= \case
    Nothing -> pure $ Left Errors.notFound
    Just (P.Entity quoteId DBInsuranceQuote {..}) -> do
      createPolicyTxn now dBInsuranceQuoteApplicationId quoteId

------------------------------------------------------------------------------

-- |
createApplicationTxn ::
  MonadIO m =>
  InsuranceApplication ->
  ReaderT P.SqlBackend m (DBInsuranceApplicationId)
createApplicationTxn application = do
  now <- liftIO getCurrentTime
  uuid <- liftIO $ InsuranceUUID <$> UUID.nextRandom
  P.insert $ toDBInsuranceApplication now uuid application

------------------------------------------------------------------------------

createPolicyTxn ::
  MonadIO m =>
  UTCTime ->
  DBInsuranceApplicationId ->
  DBInsuranceQuoteId ->
  ReaderT P.SqlBackend m (Either Errors.APIError InsuranceEndpointResponse)
createPolicyTxn now applicationId quoteId = do
  uuid <- liftIO $ InsuranceUUID <$> UUID.nextRandom
  P.insert_ $ toDBInsurancePolicy now uuid applicationId quoteId
  pure . Right $
    InsuranceEndpointResponse
      "Created policy, see the \"uuid\" field if you need its UUID"
      (Just uuid)

------------------------------------------------------------------------------

-- |
createQuoteTxn ::
  MonadIO m =>
  InsuranceApplication ->
  ReaderT P.SqlBackend m InsuranceEndpointResponse
createQuoteTxn application = do
  now <- liftIO getCurrentTime
  uuid <- liftIO $ InsuranceUUID <$> UUID.nextRandom
  applicationId <- createApplicationTxn application
  P.insert_ $ toDBInsuranceQuote now uuid applicationId application
  pure $
    InsuranceEndpointResponse
      "Created quote, see the \"uuid\" field if you need its UUID"
      (Just uuid)

------------------------------------------------------------------------------

-- |
discardQuoteTxn ::
  MonadIO m =>
  InsuranceUUID ->
  ReaderT P.SqlBackend m (Either Errors.APIError InsuranceEndpointResponse)
discardQuoteTxn quoteUUID = do
  now <- liftIO getCurrentTime
  P.updateWhere
    [DBInsuranceQuoteUuid P.==. quoteUUID]
    [ DBInsuranceQuoteStatus P.=. QuoteStatus_Discarded,
      DBInsuranceQuoteStatusUpdatedAt P.=. now
    ]
  pure . Right $
    InsuranceEndpointResponse
      "Discarded quote, see the \"uuid\" field if you need its UUID"
      (Just quoteUUID)

------------------------------------------------------------------------------

-- |
retrieveInsurancePolicyTxn ::
  MonadIO m =>
  InsuranceUUID ->
  ReaderT P.SqlBackend m (Either Errors.APIError APIInsuranceResponse)
retrieveInsurancePolicyTxn policyUUID = runExceptT $ do
  P.Entity _ policy@DBInsurancePolicy {..} <-
    ExceptT $
      note Errors.notFound
        <$> P.selectFirst [DBInsurancePolicyUuid P.==. policyUUID] []

  P.Entity _ quote <-
    ExceptT $
      note Errors.notFound
        <$> P.selectFirst [DBInsuranceQuoteId P.==. dBInsurancePolicyQuoteId] []

  P.Entity _ application <-
    ExceptT $
      note Errors.notFound
        <$> P.selectFirst [DBInsuranceApplicationId P.==. dBInsurancePolicyApplicationId] []

  pure $ APIInsuranceResponse application quote policy

------------------------------------------------------------------------------

-- |
toDBInsuranceApplication ::
  UTCTime ->
  InsuranceUUID ->
  InsuranceApplication ->
  DBInsuranceApplication
toDBInsuranceApplication now dBInsuranceApplicationUuid InsuranceApplication {..} =
  let dBInsuranceApplicationApplicantName = _insuranceApplication_applicantName
      dBInsuranceApplicationInsuranceStartDate = _insuranceApplication_insuranceStartDate
      dBInsuranceApplicationInsuranceEndDate = _insuranceApplication_insuranceEndDate
      dBInsuranceApplicationInsuredItemPrice = _insuranceApplication_insuredItemPrice
      dBInsuranceApplicationAppliedAt = now
   in DBInsuranceApplication {..}

------------------------------------------------------------------------------

-- |
toDBInsurancePolicy ::
  UTCTime ->
  InsuranceUUID ->
  DBInsuranceApplicationId ->
  DBInsuranceQuoteId ->
  DBInsurancePolicy
toDBInsurancePolicy now uuid dBInsurancePolicyApplicationId dBInsurancePolicyQuoteId = do
  let dBInsurancePolicyUuid = uuid
      dBInsurancePolicyPurchasedAt = now
   in DBInsurancePolicy {..}

------------------------------------------------------------------------------

-- |
toDBInsuranceQuote ::
  UTCTime ->
  InsuranceUUID ->
  DBInsuranceApplicationId ->
  InsuranceApplication ->
  DBInsuranceQuote
toDBInsuranceQuote now quoteUUID applicationId InsuranceApplication {..} =
  let dBInsuranceQuoteUuid = quoteUUID
      dBInsuranceQuoteApplicationId = applicationId
      dBInsuranceQuoteInsuranceCost = calculateCost (_insuranceApplication_insuredItemPrice)
      dBInsuranceQuoteStatus = QuoteStatus_Pending
      dBInsuranceQuoteStatusUpdatedAt = now
   in DBInsuranceQuote {..}
  where
    calculateCost price = price `quot` 5
