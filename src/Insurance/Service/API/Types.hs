-- |
module Insurance.Service.API.Types
  ( -- *
    APIInsuranceResponse (..),
  )
where

------------------------------------------------------------------------------

import Data.Aeson (ToJSON)
import qualified Data.Aeson as A
import Insurance.DB.Schema

------------------------------------------------------------------------------

-- |
data APIInsuranceResponse
  = APIInsuranceResponse DBInsuranceApplication DBInsuranceQuote DBInsurancePolicy

-- We don't really have to serialize all these fields but putting them in here
-- anyway imagining a scenerio where we want to have access to a unique identifier
-- throughout the process.
instance ToJSON APIInsuranceResponse where
  toJSON (APIInsuranceResponse application quote policy) =
    A.object
      [ "applicationUUID" A..= dBInsuranceApplicationUuid application,
        "quoteUUID" A..= dBInsuranceQuoteUuid quote,
        "policyUUID" A..= dBInsurancePolicyUuid policy,
        "insuranceStartDate" A..= dBInsuranceApplicationInsuranceStartDate application,
        "insuranceEndDate" A..= dBInsuranceApplicationInsuranceEndDate application,
        "insuredItemPrice" A..= dBInsuranceApplicationInsuredItemPrice application,
        "insuranceCost" A..= dBInsuranceQuoteInsuranceCost quote,
        "purchasedAt" A..= dBInsurancePolicyPurchasedAt policy
      ]
