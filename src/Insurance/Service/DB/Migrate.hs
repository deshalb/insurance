module Insurance.Service.DB.Migrate
  ( migrate,
  )
where

import Control.Monad.Logger (NoLoggingT (..))
import Control.Monad.Reader
import Control.Monad.Trans.Resource (runResourceT)
import qualified Database.Persist.Postgresql as P
import Database.Persist.Sql as P hiding (migrate)
import Database.PostgreSQL.Simple
import Insurance.DB.Schema

------------------------------------------------------------------------------

migrate :: ConnectInfo -> IO ()
migrate connInfo =
  runNoLoggingT . runResourceT $
    P.withPostgresqlConn (postgreSQLConnectionString connInfo) $
      runReaderT $ runMigration migrateAll
