{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

-- |
module Insurance.Service.DB.Types
  ( -- *
    InsuranceUUID (..),
  )
where

------------------------------------------------------------------------------

import Data.Aeson (ToJSON)
import qualified Data.ByteString.Char8 as C
import Data.Text (Text)
import Data.UUID (UUID)
import qualified Data.UUID as UUID
import Database.Persist.Sql
import Web.HttpApiData

------------------------------------------------------------------------------

-- |
newtype InsuranceUUID = InsuranceUUID {_insuranceUUID :: UUID}
  deriving stock (Eq, Show)
  -- We'd normally define `ToJSON` and `FromJSON` instances manually to
  -- have preference over serialization without messing with out naming
  -- standards.
  deriving newtype (ToJSON)

instance PersistFieldSql InsuranceUUID where
  sqlType _ = SqlOther "uuid"

instance PersistField InsuranceUUID where
  toPersistValue :: InsuranceUUID -> PersistValue
  toPersistValue (InsuranceUUID uuid) =
    PersistLiteral_ Escaped . C.pack . UUID.toString $ uuid

  fromPersistValue :: PersistValue -> Either Text InsuranceUUID
  fromPersistValue (PersistLiteral_ _ uuidB8) =
    case UUID.fromString $ C.unpack uuidB8 of
      Just uuid -> Right $ InsuranceUUID uuid
      Nothing -> Left "Invalid UUID"
  fromPersistValue _ = Left "Not PersistLiteral_"

instance ToHttpApiData InsuranceUUID where
  toUrlPiece :: InsuranceUUID -> Text
  toUrlPiece (InsuranceUUID uuid) = UUID.toText uuid

instance FromHttpApiData InsuranceUUID where
  parseUrlPiece :: Text -> Either Text InsuranceUUID
  parseUrlPiece txt = case UUID.fromText txt of
    Nothing -> Left "Invalid UUID"
    Just uuid -> Right $ InsuranceUUID uuid
