-- |
module Insurance.Service.API
  ( -- *
    InsuranceAPI,
    insuranceAPI,
    migrate,
    server,
  )
where

------------------------------------------------------------------------------

import Control.Monad.Reader
import Insurance.DB.Persist (connInfo, runDB)
import Insurance.Service.API.Types
import Insurance.Service.DB
import Insurance.Service.DB.Migrate (migrate)
import Insurance.Service.DB.Types
import Insurance.Service.Types
import Insurance.Web.Errors
import Servant

------------------------------------------------------------------------------

-- | Insurance API defitinion.
type InsuranceAPI =
  "generateQuote" :> ReqBody '[JSON] InsuranceApplication :> Post '[JSON] InsuranceEndpointResponse
    :<|> "discardQuote" :> Capture "quoteUUID" InsuranceUUID :> Patch '[JSON] (Either APIError InsuranceEndpointResponse)
    :<|> "acceptQuote" :> Capture "quoteUUID" InsuranceUUID :> Patch '[JSON] (Either APIError InsuranceEndpointResponse)
    :<|> "retrieveInsurancePolicy" :> Capture "policyUUID" InsuranceUUID :> Get '[JSON] (Either APIError APIInsuranceResponse)
    :<|> "admin" :> "migrate" :> Post '[JSON] InsuranceEndpointResponse

------------------------------------------------------------------------------

-- |
insuranceAPI :: Proxy InsuranceAPI
insuranceAPI = Proxy

------------------------------------------------------------------------------

-- |
server :: Server InsuranceAPI
server =
  generateQuoteH
    :<|> discardQuoteH
    :<|> acceptQuoteH
    :<|> retrieveInsurancePolicyH
    :<|> migrateH

------------------------------------------------------------------------------

-- |
generateQuoteH ::
  InsuranceApplication ->
  Handler InsuranceEndpointResponse
generateQuoteH application = do
  runDB $ createQuoteTxn application

------------------------------------------------------------------------------

-- | This and `acceptQuoteH` can just return `Maybe APIError` or
-- a successful status on the right.
discardQuoteH ::
  InsuranceUUID ->
  Handler (Either APIError InsuranceEndpointResponse)
discardQuoteH quoteUUID =
  runDB $ discardQuoteTxn quoteUUID

------------------------------------------------------------------------------

-- |
acceptQuoteH ::
  InsuranceUUID ->
  Handler (Either APIError InsuranceEndpointResponse)
acceptQuoteH quoteUUID =
  runDB $ acceptQuoteTxn quoteUUID

------------------------------------------------------------------------------

-- |
retrieveInsurancePolicyH ::
  InsuranceUUID ->
  Handler (Either APIError APIInsuranceResponse)
retrieveInsurancePolicyH policyUUID =
  runDB $ retrieveInsurancePolicyTxn policyUUID

------------------------------------------------------------------------------

-- |
migrateH :: Handler InsuranceEndpointResponse
migrateH = do
  liftIO $ migrate connInfo
  pure $ InsuranceEndpointResponse "Migration successful" Nothing
