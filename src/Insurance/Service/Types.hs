{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

-- |
module Insurance.Service.Types
  ( -- *
    QuoteStatus (..),

    -- * Request body types
    InsuranceApplication (..),

    -- * Response body types
    InsuranceEndpointResponse (..),
  )
where

------------------------------------------------------------------------------

import Control.Error.Util (note)
import Control.Lens
import Data.Aeson (FromJSON, ToJSON)
import qualified Data.Aeson as A
import Data.Text as T
import Data.Time (UTCTime)
import Data.Word (Word64)
import Database.Persist.Sql
import Insurance.Service.DB.Types (InsuranceUUID)

------------------------------------------------------------------------------

-- |
data QuoteStatus
  = QuoteStatus_Pending
  | QuoteStatus_Accepted
  | QuoteStatus_Discarded

quoteStatusText :: Prism' Text QuoteStatus
quoteStatusText = prism' toT fromT
  where
    toT = \case
      QuoteStatus_Pending -> "Pending"
      QuoteStatus_Accepted -> "Accepted"
      QuoteStatus_Discarded -> "Discarded"
    fromT = \case
      "Pending" -> Just QuoteStatus_Pending
      "Accepted" -> Just QuoteStatus_Accepted
      "Discarded" -> Just QuoteStatus_Discarded
      _ -> Nothing

instance ToJSON QuoteStatus where
  toJSON quoteStatus = A.String $ review quoteStatusText quoteStatus

instance PersistField QuoteStatus where
  toPersistValue q = PersistText $ review quoteStatusText q

  fromPersistValue (PersistText t) = note ("Can't parse text: " <> t) $ preview quoteStatusText t
  fromPersistValue value = Left $ "Invalid value for QuoteStatus: " <> (T.pack $ show value)

instance PersistFieldSql QuoteStatus where
  sqlType _ = SqlString

------------------------------------------------------------------------------

-- |
data InsuranceApplication = InsuranceApplication
  { _insuranceApplication_applicantName :: Text,
    _insuranceApplication_insuranceStartDate :: UTCTime,
    _insuranceApplication_insuranceEndDate :: UTCTime,
    _insuranceApplication_insuredItemPrice :: Word64
  }

instance FromJSON InsuranceApplication where
  parseJSON = A.withObject "InsuranceApplication" $ \o -> do
    _insuranceApplication_applicantName <- o A..: "applicantName"
    _insuranceApplication_insuranceStartDate <- o A..: "insuranceStartDate"
    _insuranceApplication_insuranceEndDate <- o A..: "insuranceEndDate"
    _insuranceApplication_insuredItemPrice <- o A..: "insuredItemPrice"
    pure $
      InsuranceApplication {..}

------------------------------------------------------------------------------

-- | Response body type for insurance API endpoints except for the one
-- retrieving policies.
data InsuranceEndpointResponse = InsuranceEndpointResponse
  { _insuranceEndpointResponse_responseMessage :: Text,
    _insuranceEndpointResponse_uuid :: Maybe InsuranceUUID
  }

instance ToJSON InsuranceEndpointResponse where
  toJSON InsuranceEndpointResponse {..} =
    A.object $
      [ "responseMessage" A..= _insuranceEndpointResponse_responseMessage
      ]
        <> omitIfNullAndSerialize
    where
      omitIfNullAndSerialize =
        case _insuranceEndpointResponse_uuid of
          Nothing -> []
          Just uuid -> ["uuid" A..= uuid]
