{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE UndecidableInstances #-}

-- |
module Insurance.DB.Schema where

------------------------------------------------------------------------------

import Data.Text (Text)
import Data.Time (UTCTime)
import Data.Word
import Database.Persist.Quasi (lowerCaseSettings)
import Database.Persist.Sql
import Database.Persist.TH
import Insurance.Service.DB.Types (InsuranceUUID)
import Insurance.Service.Types hiding (InsuranceApplication)

------------------------------------------------------------------------------

share
  [mkPersist sqlSettings, mkMigrate "migrateAll"]
  $(persistFileWith lowerCaseSettings "schema/Insurance.persistentmodels")
