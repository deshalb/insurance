module Insurance.DB.Persist
  ( -- *
    runDB,

    -- *
    connInfo,
  )
where

-------------------------------------------------------------------------------

import Control.Monad.Logger (NoLoggingT (..))
import Control.Monad.Reader (liftIO)
import Control.Monad.Trans.Resource (ResourceT, runResourceT)
import qualified Database.Persist.Postgresql as P
import Database.Persist.Sql as P hiding (migrate)
import Database.PostgreSQL.Simple (ConnectInfo (..))
import qualified Database.PostgreSQL.Simple as PS
import Servant (Handler)

-------------------------------------------------------------------------------

-- | Run a database operation, and lift the result into a Handler.
-- This minimizes the usage of IO operations in other functions.
runDB :: P.SqlPersistT (ResourceT (NoLoggingT IO)) a -> Handler a
runDB a =
  liftIO . runNoLoggingT . runResourceT $
    P.withPostgresqlConn (PS.postgreSQLConnectionString connInfo) $ runSqlConn a

-------------------------------------------------------------------------------

-- | TODO(dalp): This will need come from a configuration file.
connInfo :: PS.ConnectInfo
connInfo =
  PS.defaultConnectInfo
    { connectHost = "127.0.0.1",
      connectUser = "testUser",
      -- Setting the port to 5433 to avoid clashing with existing instances
      -- of PostgreSQL
      connectPort = 5433,
      connectPassword = "1234",
      connectDatabase = "insurance"
    }
