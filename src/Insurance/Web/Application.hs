-- |
module Insurance.Web.Application
  ( app,
  )
where

------------------------------------------------------------------------------

import Insurance.Service.API (insuranceAPI, server)
import Servant (Application, serve)

------------------------------------------------------------------------------

-- |
app :: Application
app = serve insuranceAPI server
