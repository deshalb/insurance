-- |
module Insurance.Web.Errors
  ( -- *
    APIError (..),

    -- *
    notFound,
  )
where

------------------------------------------------------------------------------

import Data.Aeson (ToJSON)
import qualified Data.Aeson as A
import Data.Text as T
import qualified Network.HTTP.Types.Status as NHTS

------------------------------------------------------------------------------

-- |
data APIError = APIError
  { _apiError_status :: NHTS.Status,
    _apiError_title :: Text,
    _apiError_detail :: Text,
    _apiError_internalDetail :: Maybe Text
  }

instance ToJSON APIError where
  toJSON APIError {..} =
    A.object
      [ "status" A..= show _apiError_status,
        "title" A..= _apiError_title,
        "detail" A..= _apiError_detail,
        "internalDetail" A..= _apiError_internalDetail
      ]

notFound :: APIError
notFound =
  APIError
    { _apiError_status = NHTS.notFound404,
      _apiError_title = "NotFound",
      _apiError_detail = "Not Found",
      _apiError_internalDetail = Nothing
    }
