# insurance

## For developers
You can lint the project with:
```
ormolu --mode inplace $(find src test -name '*.hs')
```

## Usage

### Prerequisites
* [Nix](https://nixos.wiki/wiki/Nix)

### Running
To get a docker container running PostgreSQL configured to work with
the `insurance-core` executable provided by this project you'll need
to follow the steps below.

For more information take a look at the `docker-compose.yaml` file at
the root of the project.

```
user@machine:~/src$ git clone git@gitlab.com:deshalb/insurance.git
user@machine:~/src$ cd insurance
user@machine:~/src/insurance$ nix-shell
> insurance.cabal is up-to-date
[nix-shell:~/src/insurance]$ docker-compose up
```

Then in another shell instance:
```
user@machine:~/src/insurance$ nix-shell
> insurance.cabal is up-to-date
[nix-shell:~/src/insurance]$ cabal new-run insurance-core
```

Once you get the executable running the first thing you have to do is to
run the migrations to populate the database, which can be done in two ways:
1) (Recommended) Send a `POST` request to `127.0.0.1:8080/admin/migrate`
2) Run `cabal new-run insurance-core migrate`

Now take a look at the `Testing` section to start trying out endpoints.

## Testing
You can import the Postman collection defined by the JSON file named `Insurance.postman_collection.json`
at the root of the project. To sum it up:

* `POST` `127.0.0.1:8080/generateQuote` with an appropriate JSON body, this will
return you a UUID belonging to the generated quote that you'll use in the endpoint below

* `PATCH` `127.0.0.1:8080/acceptQuote/:quoteUUID`, this will update the quote's status
to accepted, updates the updatedAt field and crete an insurance policy and return its
identifier UUID which you then can supply at the next endpoint to look at the policy

* `GET` `127.0.0.1:8080/retrieveInsurancePolicy/:policyUUID`, given that policy
policyUUID is a valid UUID and one that exists in PostgreSQL it will return
some information about the policy

* `PATCH` `127.0.0.1:8080/discardQuote/:quoteUUID` will discard the quote, which
simply means that it'll set it's status to `Discarded` and replace the updatedAt field
with the current time; we could also delete the quotes if need be