module Main
  ( main
  )
where

------------------------------------------------------------------------------

import Insurance.Web.Application (app)
import Insurance.Service.DB.Migrate (migrate)
import Safe
import System.Environment (getArgs)
import Network.Wai.Handler.Warp
import Insurance.DB.Persist (connInfo)

------------------------------------------------------------------------------

-- | Long list of TODOs:
-- * Give this an `optparse-applicative` CLI or just pull the
-- migration to a seperate executable
-- * Construct ReaderT app stack, have the config come from a yaml or dhall
-- file and add a bunch of combinators to work with the stack
-- * Add authorization, GDP style
-- * Add logging using katip
-- * Add instrumentation using instrument
-- and more I'm not listing out here for this to be prod ready ...

main :: IO ()
main = do
  args <- getArgs
  let arg1 = if not (null args) then headMay args else Nothing
  case arg1 of
    Just "migrate" -> migrate connInfo
    _              -> do
      print "App running"
      run 8080 app
