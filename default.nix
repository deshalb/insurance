{ compiler ? "ghc8104" }:

let
  sources = import ./nix/sources.nix;
  pkgs = import sources.nixpkgs { };

  inherit (pkgs.haskell.lib) dontCheck;

  baseHaskellPkgs = pkgs.haskell.packages.${compiler};

  # TODO(dalp): Pull this overlay out into its own file
  myHaskellPackages = baseHaskellPkgs.override {
    overrides = hself: hsuper: {
      insurance = hself.callCabal2nix "insurance" (./.) { };
      esqueleto = dontCheck hself.esqueleto_3_4_2_1;
      persistent = dontCheck hself.persistent_2_13_0_1;
      persistent-postgresql = dontCheck hself.persistent-postgresql_2_13_0_0;
      persistent-template = dontCheck hself.persistent-template_2_12_0_0;
      
    };
  };

  shell = myHaskellPackages.shellFor {
    packages = p: with p; [
      insurance
    ];

    buildInputs = with pkgs.haskellPackages; [
      cabal-install
      ghcid
      ormolu
      hlint
      pkgs.niv
      pkgs.nixpkgs-fmt
    ];

    libraryHaskellDepends = [
    ];

    shellHook = ''
      set -e
      hpack
      set +e
    '';
  };

in
{
  inherit shell;
  # Useful for checking versions of packages available in the current set
  inherit myHaskellPackages;
  insurance = myHaskellPackages.insurance;
}
